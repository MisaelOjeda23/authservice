import type { Ref } from "vue";
import { ref } from "vue";
import type IUsers from "@/interface/IUsers";

const url = import.meta.env.API_URL

export default class AuthService {
    private users: Ref<IUsers[]>
    private user: Ref<IUsers>

    constructor() {
        this.users = ref([])
        this.user = ref() as Ref<IUsers>
    }
    
    getUsers(): Ref<IUsers[]> {
        return this.users
    }

    getUser(): Ref<IUsers> {
        return this.user
    }

    async fetchAll(): Promise<void> {
        try {
            const json = await fetch(url)
            const response = await json.json()
            this.users.value = await response
        }catch (error) {
            console.log(error)
        }
    }

    async fetchUser(email: string): Promise<void> {
        try {
            const json = await fetch(url + '/User?email=' + email)
            const response = await json.json()
            this.user.value = await response
        } catch (error) {
            console.log(error)
        }
    } 

    async postUser(email: string, password: string, name: string) {
        try {
            const response = await fetch(url + '/register?email=' + email + '&password=' + password + '&name=' + name, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": '*'
                },
            })
            const result = await response.json()
            console.log("Success: ", result)
        } catch (error) {
            console.log(error)
        }
    }
}